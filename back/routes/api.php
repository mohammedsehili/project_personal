<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Collection;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/////Looooooooooooooooogin
Route::post('/LoginAdmin/', function () {
    $data = request()->all();
    $mailExist = DB::select('select * from Admin where upper(Email_Admin)=upper(:Email_Admin)', [
        'Email_Admin' =>$data['Email_Admin'],
     ]);
    $mailExist = sizeof($mailExist);
    if($mailExist == 0 ){ 
    $results= [ 
        'log_mensajes' => 'correo incorrecto',
    ];
    return response()->json($results, 200); 
    }else{  
        $passwordTrue = DB::select('select Pssword_Admin from Admin where upper(Email_Admin)=upper(:Email_Admin)', [
            'Email_Admin' =>$data['Email_Admin'],
            ]);
        $arr = []; 
        foreach($passwordTrue as $raw){
            $arr[]=(array)$raw;
        }
    if($data['Pssword_Admin']==$arr[0]['Pssword_Admin']){
    $results = DB::select('select idadmin, role from Admin where upper(Email_Admin)=upper(:Email_Admin) ', [
        'Email_Admin' =>$data['Email_Admin'],
    ]);
    return response()->json($results[0], 200); 
    }else
    $results= 
    [ 
    'log_mensajes' => 'contraseña incorrecta',
    ];
       return response()->json($results, 200);
    }
});










Route::get('/cities', function (Request $request) {
    $results = DB::select('select * from cities');
    return response()->json($results, 200);
});

Route::get('/editcity/{idcity}', function ($idcity) {
    $results = DB::select('select * from cities where idcity=:idcity ', [
        'idcity' => $idcity,
    ]);
    return response()->json($results[0], 200);
});

Route::POST('/cities', function () {
   
    $data = request()->all();
    DB::insert(
        'INSERT into cities(id_city,name_city)  VALUES (:id_city,:name_city)',
        $data
    );
   
    $results = DB::select('select * from cities where id_city = :id_city', [
        'id_city' => $data['id_city'],
    ]);
    return response()->json($results[0], 200);
});

Route::delete('/cities/{idcity}', function ($idcity) {
    $data = request()->all();
    DB::delete('delete from cities where idcity=:idcity ', [
          'idcity' => $idcity,
      ]);
  
     $results= [ 
      'succesful' => 'City deleted',
  ];
     
      return response()->json($results, 200);
  });


  Route::PUT('/updatecities/{idcity}', function (Request $request, $idcity) {
    
    DB::update("update cities set  id_city=?, name_city=? where idcity=?",
    [$request->id_city,
    $request->name_city,
    $request->idcity,
    ]);

    $results = DB::select('select * from cities where idcity = :idcity', ['idcity' => $idcity]);
    return response()->json($results, 200);
});

/*
  Route::PUT('/updatecities/{idcity}', function (Request $request, $idcity) {
    
    $data = request()->all();
    if (idcityNoExists($idcity)) {
       abort(404);
   }

var_dump($idcity);
 DB::update("update cities set  id_city=:id_city, name_city=:name_city WHERE idcity=:idcity",
  [
    'id_city'=>$data['id_city'],
    'name_city'=>$data['name_city'],
     'idcity'=>$idcity
 ]);
$results = DB::select('select * from cities where idcity=:idcity', ['idcity' => $idcity]);
return response()->json($results[0], 200);
});*/

if (!function_exists('idcityNoExists')) {
    function idcityNoExists($idcity)
    {
        $results = DB::select('select * from cities where idcity=:idcity', [
            'idcity' => $idcity,
        ]);

        return count($results) == 0;
    }
}
/****************************************************************** */

Route::get('/services', function (Request $request) {
    $results = DB::select('select * from services');
    return response()->json($results, 200);
});


Route::get('/editservices/{idservice}', function ($idservice) {
    $results = DB::select('select * from services where idservice=:idservice', [
        'idservice' => $idservice,
    ]);
    return response()->json($results[0], 200);
});

Route::POST('/services', function () {
   
    $data = request()->all();
    DB::insert(
        'INSERT into services(id_services,name_services)  VALUES (:id_services,:name_services)',
        $data
    );
   
    $results = DB::select('select * from services where id_services = :id_services', [
        'id_services' => $data['id_services'],
    ]);
    return response()->json($results[0], 200);
});





Route::delete('/services/{idservice}', function ($idservice) {
    $data = request()->all();
    DB::delete('delete from services where idservice=:idservice ', [
          'idservice' => $idservice,
      ]);
  
     $results= [ 
      'succesful' => 'Service deleted',
  ];
     
      return response()->json($results, 200);
  });



  Route::PUT('/updateService/{idservice}', function (Request $request, $idservice) {
    
    $data = request()->all();
    if (idserviceNoExists($idservice)) {
       abort(404);
   }


 DB::update("update services set  id_services=:id_services, name_services=:name_services WHERE idservice=:idservice",
  [
    'id_services'=>$data['id_services'],
    'name_services'=>$data['name_services'],
     'idservice'=>$idservice
 ]);
$results = DB::select('select * from services where idservice=:idservice', ['idservice' => $idservice]);
return response()->json($results[0], 200);
});

if (!function_exists('idserviceNoExists')) {
    function idserviceNoExists($idservice)
    {
        $results = DB::select('select * from services where idservice=:idservice', [
            'idservice' => $idservice,
        ]);

        return count($results) == 0;
    }
}








  /******************************************************************** */
  Route::get('/orders', function (Request $request) {
    $results = DB::select('select * from orders');
    return response()->json($results, 200);
});




Route::POST('/orders', function () {
   
    $data = request()->all();
    DB::insert(
        'INSERT into orders(nameService,nameCity,name_orders,email_orders,phone_ordres,notes_ordres,date_orders)  VALUES (:nameService,:nameCity,:name_orders,:email_orders,:phone_ordres,:notes_ordres,:date_orders)',
        $data
    );
   
    $results = DB::select('select * from orders where nameCity = :nameCity', [
        'nameCity' => $data['nameCity'],
    ]);
    return response()->json($results[0], 200);
});


Route::delete('/orders/{idorders}', function ($idorders) {
    $data = request()->all();
    DB::delete('delete from orders where idorders=:idorders', [
          'idorders' => $idorders,
      ]);
  
     $results= [ 
      'succesful' => 'orders deleted',
  ];
     
      return response()->json($results, 200);
  });
  /********************************************************************** */

  Route::get('/Admin', function (Request $request) {
    $results = DB::select('select * from Admin');
    return response()->json($results, 200);
});


Route::get('/editadmin/{idadmin}', function ($idadmin) {
    $results = DB::select('select * from Admin where idadmin=:idadmin ', [
        'idadmin' => $idadmin,
    ]);
    return response()->json($results[0], 200);
});



Route::POST('/Admin', function () {
   
    $data = request()->all();
    DB::insert(
        'INSERT into Admin(id_Admin,name_Admin,Email_Admin,Pssword_Admin)  VALUES (:id_Admin,:name_Admin,:Email_Admin,:Pssword_Admin)',
        $data
    );
   
    $results = DB::select('select * from Admin where id_Admin = :id_Admin', [
        'id_Admin' => $data['id_Admin'],
    ]);
    return response()->json($results[0], 200);
});


Route::delete('/Admin/{idadmin}', function ($idadmin) {
    $data = request()->all();
    DB::delete('delete from Admin where idadmin=:idadmin', [
          'idadmin' => $idadmin,
      ]);
  
     $results= [ 
      'succesful' => 'orders deleted',
  ];
     
      return response()->json($results, 200);
  });


  Route::PUT('/updateAdmin/{idadmin}', function (Request $request, $idadmin) {
    
    $data = request()->all();
    if (idadminNoExists($idadmin)) {
       abort(404);
   }


 DB::update("update Admin set  id_Admin=:id_Admin, name_Admin=:name_Admin,Email_Admin=:Email_Admin,Pssword_Admin=:Pssword_Admin WHERE idadmin=:idadmin",
  [
    'id_Admin'=>$data['id_Admin'],
    'name_Admin'=>$data['name_Admin'],
    'Email_Admin'=>$data['Email_Admin'],
    'Pssword_Admin'=>$data['Pssword_Admin'],
     'idadmin'=>$idadmin
 ]);
$results = DB::select('select * from Admin where idadmin=:idadmin', ['idadmin' => $idadmin]);
return response()->json($results[0], 200);
});

if (!function_exists('idadminNoExists')) {
    function idadminNoExists($idadmin)
    {
        $results = DB::select('select * from Admin where idadmin=:idadmin', [
            'idadmin' => $idadmin,
        ]);

        return count($results) == 0;
    }
}
