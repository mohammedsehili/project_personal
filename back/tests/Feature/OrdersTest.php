<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class OrdersTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            
            CREATE TABLE  orders (
                idorders	    TEXT NOT NULL,
                nameService	TEXT,
                nameCity	    TEXT,
                id_orders Text,
                name_orders Text ,
                email_orders Text,
                phone_ordres Text,
                notes_ordres Text,
                date_orders Text
               
            );
         INSERT INTO orders VALUES ('1','test', 'tanger','Y895632','Mohammed sehili' ,'med95sehili@gmail.com','6858666','estoy enfermo', '2020-01-01');
         ");
      
    }


    public function testGetorders()
    {
        $this->json('GET', 'api/orders')
            ->assertStatus(200)
            ->assertJson([
                [
                    'idorders' => '1',
                    'nameService' => 'test',
                    'nameCity' => 'tanger',
                    'id_orders'=>'Y895632',
                    'name_orders'=>'Mohammed sehili',
                    'email_orders'=>'med95sehili@gmail.com',
                    'phone_ordres'=>'6858666',
                    'notes_ordres'=>'estoy enfermo',
                    'date_orders'=>'2020-01-01'

                   
                ]
                
                ]
            );
    }
    public function testDeleteorders()
    {
     //   $this->json('GET', 'api/cars/1')->assertStatus(200);

        $this->json('DELETE', 'api/orders/1')->assertStatus(200);

       // $this->json('GET', 'api/cars/1')->assertStatus(404);
    }



    



/*
    
   public function testPostCities()
    {
        $this->json('POST', 'api/cities', [
                    'idcity' => '99',
                    'name_city' => 'alava',
                    'id_city' => '01VI',
        ])->assertStatus(200);

        $this->json('GET', 'api/cities/99')
            ->assertStatus(200)
            ->assertJson([
                'idcity' => '99',
                'name_city' => 'alava',
                'id_city' => '01VI',
            ]);
    }*/
}