<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class CitiesTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            
            CREATE TABLE  cities (
                idcity	    TEXT NOT NULL,
                name_city	TEXT,
                id_city	    TEXT
               
            );
         INSERT INTO cities VALUES ('22','alava', '01VI');
            INSERT INTO cities VALUES ('23','Albacete', '02AB');
        ");
      
    }


    public function testGetCities()
    {
        $this->json('GET', 'api/cities')
            ->assertStatus(200)
            ->assertJson([
                [
                    'idcity' => '22',
                    'name_city' => 'alava',
                    'id_city' => '01VI',
                   
                ]
                ,
                [
                    'idcity' => '23',
                    'name_city' => 'Albacete',
                    'id_city' => '02AB',
                ],
                ]
            );
    }
    public function testDeleteCities()
    {
     //   $this->json('GET', 'api/cars/1')->assertStatus(200);

        $this->json('DELETE', 'api/cities/1')->assertStatus(200);

       // $this->json('GET', 'api/cars/1')->assertStatus(404);
    }



    public function testPut()
    {
        $data = [
            'idcity' => '22',
            'name_city' => 'alava',
            'id_city' => '01VI',
            
        ];

        $expected = [
            'idcity' => '22',
            'name_city' => 'alava',
            'id_city' => '01VI',
           
        ];

        $this->json('PUT', 'api/updatecities/22', $data)
            ->assertStatus(200)
            ->assertJson($expected);

       /* $this->json('GET', 'api/updatecities/22')
            ->assertStatus(200)
            ->assertJson($expected);*/
    }



/*
    
   public function testPostCities()
    {
        $this->json('POST', 'api/cities', [
                    'idcity' => '99',
                    'name_city' => 'alava',
                    'id_city' => '01VI',
        ])->assertStatus(200);

        $this->json('GET', 'api/cities/99')
            ->assertStatus(200)
            ->assertJson([
                'idcity' => '99',
                'name_city' => 'alava',
                'id_city' => '01VI',
            ]);
    }*/
}