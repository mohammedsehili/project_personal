<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class AdminTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            
            CREATE TABLE  Admin (
                idadmin	    TEXT NOT NULL,
                id_Admin	TEXT,
                name_Admin	    TEXT,
                Email_Admin Text,
                Pssword_Admin Text ,
                role Text
               
            );
         INSERT INTO Admin VALUES ('1','Y7565', 'Mohammed sehili','med95sehili@gmail.com','123' ,'Admin');
         INSERT INTO Admin VALUES ('2','Y758465', 'Hamza sehili','hamza20sehili@gmail.com','123' ,'Admin');
         ");
      
    }


    public function testGetAdmin()
    {
        $this->json('GET', 'api/Admin')
            ->assertStatus(200)
            ->assertJson([
                [
                    'idadmin' => '1',
                    'id_Admin' => 'Y7565',
                    'name_Admin' => 'Mohammed sehili',
                    'Email_Admin'=>'med95sehili@gmail.com',
                    'Pssword_Admin'=>'123',
                    'role'=>'Admin'
                   
                ]
                ,
                [
                    'idadmin' => '2',
                    'id_Admin' => 'Y758465',
                    'name_Admin' => 'Hamza sehili',
                    'Email_Admin'=>'hamza20sehili@gmail.com',
                    'Pssword_Admin'=>'123',
                    'role'=>'Admin'
                ],
                ]
            );
    }
    public function testDeleteAdmin()
    {
     //   $this->json('GET', 'api/cars/1')->assertStatus(200);

        $this->json('DELETE', 'api/Admin/1')->assertStatus(200);

       // $this->json('GET', 'api/cars/1')->assertStatus(404);
    }



    public function testPut()
    {
        $data = [
            'idadmin' => '1',
            'id_Admin' => 'Y7565',
            'name_Admin' => 'Mohammed sehili',
            'Email_Admin'=>'med95sehili@gmail.com',
            'Pssword_Admin'=>'123',
            'role'=>'Admin'
           
            
        ];

        $expected = [
            'idadmin' => '1',
                    'id_Admin' => 'Y7565',
                    'name_Admin' => 'Mohammed sehili',
                    'Email_Admin'=>'med95sehili@gmail.com',
                    'Pssword_Admin'=>'123',
                    'role'=>'Admin'
                   
           
        ];

        $this->json('PUT', 'api/updateAdmin/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

       /* $this->json('GET', 'api/updatecities/22')
            ->assertStatus(200)
            ->assertJson($expected);*/
    }



/*
    
   public function testPostCities()
    {
        $this->json('POST', 'api/cities', [
                    'idcity' => '99',
                    'name_city' => 'alava',
                    'id_city' => '01VI',
        ])->assertStatus(200);

        $this->json('GET', 'api/cities/99')
            ->assertStatus(200)
            ->assertJson([
                'idcity' => '99',
                'name_city' => 'alava',
                'id_city' => '01VI',
            ]);
    }*/
}