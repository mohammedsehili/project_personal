<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class ServiceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            
            CREATE TABLE  services (
                idservice	    TEXT NOT NULL,
                name_services	TEXT,
                id_services	    TEXT
               
            );
         INSERT INTO services VALUES ('1','test', 'T1');
            INSERT INTO services VALUES ('2','test2', 'T2');
        ");
      
    }


    public function testGetCities()
    {
        $this->json('GET', 'api/services')
            ->assertStatus(200)
            ->assertJson([
                [
                    'idservice' => '1',
                    'name_services' => 'test',
                    'id_services' => 'T1',
                   
                ]
                ,
                [
                    'idservice' => '2',
                    'name_services' => 'test2',
                    'id_services' => 'T2',
                ],
                ]
            );
    }
    public function testDeleteCities()
    {
     //   $this->json('GET', 'api/cars/1')->assertStatus(200);

        $this->json('DELETE', 'api/services/1')->assertStatus(200);

       // $this->json('GET', 'api/cars/1')->assertStatus(404);
    }



    public function testPut()
    {
        $data = [
            'idservice' => '1',
            'name_services' => 'test',
            'id_services' => 'T1',
            
        ];

        $expected = [
            'idservice' => '1',
            'name_services' => 'test',
            'id_services' => 'T1',
           
        ];

        $this->json('PUT', 'api/updateService/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

       /* $this->json('GET', 'api/updatecities/22')
            ->assertStatus(200)
            ->assertJson($expected);*/
    }



/*
    
   public function testPostCities()
    {
        $this->json('POST', 'api/cities', [
                    'idcity' => '99',
                    'name_city' => 'alava',
                    'id_city' => '01VI',
        ])->assertStatus(200);

        $this->json('GET', 'api/cities/99')
            ->assertStatus(200)
            ->assertJson([
                'idcity' => '99',
                'name_city' => 'alava',
                'id_city' => '01VI',
            ]);
    }*/
}