import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      component: function(resolve) {
        require(["@/components/AddOrders/Indexorders.vue"], resolve);
      },
    },

    {
      path: "/Login",
      component: function(resolve) {
        require(["@/components/Login/Login.vue"], resolve);
      },
    },
    /* {
            path: '/Nav',
            component: function(resolve) {
                require(['@/components/Nav/Nav.vue'], resolve)
            },
        },*/
    {
      path: "/Addcities",
      component: function(resolve) {
        require(["@/components/AddCities/Addcity.vue"], resolve);
      },
    },

    {
      path: "/ViewCities",
      component: function(resolve) {
        require(["@/components/Cities/Cities.vue"], resolve);
      },
    },
    {
      path: "/ViewCities/:id_actual_user",
      component: function(resolve) {
        require(["@/components/Cities/Cities.vue"], resolve);
      },
    },
    {
      path: "/Editecities/:idcity",
      component: function(resolve) {
        require(["@/components/EditCity/Editecities.vue"], resolve);
      },
    },
    {
      path: "/addservices",
      component: function(resolve) {
        require(["@/components/Addservices/Addservices.vue"], resolve);
      },
    },
    {
      path: "/viewservices",
      component: function(resolve) {
        require(["@/components/Servic/Services.vue"], resolve);
      },
    },
    {
      path: "/Editeservices/:idservice",
      component: function(resolve) {
        require(["@/components/EditService/Editeservices.vue"], resolve);
      },
    },
    {
      path: "/vieworders",
      component: function(resolve) {
        require(["@/components/Orders/Orders.vue"], resolve);
      },
    },
    {
      path: "/Addadmin",
      component: function(resolve) {
        require(["@/components/AddAdmin/Addadmin.vue"], resolve);
      },
    },
    {
      path: "/Admin",
      component: function(resolve) {
        require(["@/components/Admin/Admin.vue"], resolve);
      },
    },
    {
      path: "/Editadmine/:idadmin",
      component: function(resolve) {
        require(["@/components/EditAdmin/Editadmine.vue"], resolve);
      },
    },
  ],
});
export default router;
