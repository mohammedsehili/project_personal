const root = "/api";

//const url = '/api/cities/'
export default {
  async login(mailLogin, passwordLogin) {
    const result = await fetch(`${root}/LoginAdmin/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Email_Admin: mailLogin,
        Pssword_Admin: passwordLogin,
      }),
    }).then((response) => response.json());

    return await result;
  },
  /************************************************************************************** */
  async display_city() {
    const result = await fetch(`${root}/cities`);
    return await result.json();
  },
  async editdisplaycity(idcity) {
    const result = await fetch(`${root}/editcity/` + idcity);
    return await result.json();
  },

  async addCities(city) {
    await fetch(`${root}/cities`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_city: city.id_city,
        name_city: city.name_city,
      }),
    }).then((response) => response.json());
  },
  async deletCity(idcity) {
    await fetch(`${root}/cities/` + idcity, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  },

  async updateCity(idcities, localcity) {
    await fetch(`${root}/updatecities/` + idcities, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_city: localcity.id_city,
        name_city: localcity.name_city,
      }),
    }).then((response) => response.json());
  },
  /****************************************************************************************/

  async display_Services() {
    console.log(`${root}/services`);
    const result = await fetch(`${root}/services`);
    return await result.json();
  },

  async editdisplayservices(idservice) {
    const result = await fetch(`${root}/editservices/` + idservice);
    return await result.json();
  },

  async addService(services) {
    await fetch(`${root}/services`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_services: services.id_services,
        name_services: services.name_services,
      }),
    }).then((response) => response.json());
  },
  async DeletService(idservice) {
    await fetch(`${root}/services/` + idservice, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  },

  async updateSrevice(idservice, localservices) {
    await fetch(`${root}/updateService/` + idservice, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_services: localservices.id_services,
        name_services: localservices.name_services,
      }),
    }).then((response) => response.json());
  },
  /***********************************************************************************/
  async display_Orders() {
    let result = await fetch(`${root}/orders`);
    return await result.json();
  },

  async getOrders(orders) {
    await fetch(`${root}/orders`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nameService: orders.nameService,
        nameCity: orders.nameCity,
        name_orders: orders.name_orders,
        email_orders: orders.email_orders,
        phone_ordres: orders.phone_ordres,
        notes_ordres: orders.notes_ordres,
        date_orders: new Date().toLocaleString(),
      }),
    }).then((response) => response.json());
  },

  async getDeleteOrders(id) {
    await fetch(`${root}/orders/` + id, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  },

  /*******************************************************************************/
  async display_admin() {
    let result = await fetch(`${root}/Admin`);
    return await result.json();
  },

  async editdisplayadmin(idadmin) {
    const result = await fetch(`${root}/editadmin/` + idadmin);
    return await result.json();
  },

  async getadmin(admin) {
    await fetch(`${root}/Admin/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_Admin: admin.id_admin,
        name_Admin: admin.name_admin,
        Email_Admin: admin.Email_admin,
        Pssword_Admin: admin.Pssword_admin,
      }),
    }).then((response) => response.json());
  },

  async getDeleteAdmin(id) {
    await fetch(`${root}/Admin/` + id, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  },

  async updateAdmin(idadmin, localadmin) {
    await fetch(`${root}/updateAdmin/` + idadmin, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_Admin: localadmin.id_Admin,
        name_Admin: localadmin.name_Admin,
        Email_Admin: localadmin.Email_Admin,
        Pssword_Admin: localadmin.Pssword_Admin,
      }),
    }).then((response) => response.json());
  },
};
