import { shallowMount } from "@vue/test-utils";
import Addcity from "@/components/AddCities/Addcity.vue";

import api from "@/api.js";
const finishAsyncTasks = () => new Promise(setImmediate);

test("the input are añadir ciudades ", async () => {
  api.addCities = jest.fn();
  const wrapper = shallowMount(Addcity, {
    data() {
      return {
        cities: {
          id_city: "",
          name_city: "",
        },
      };
    },
  });
  expect(wrapper.emitted().search).toBe(undefined);
  // Simular lo que hace el usuario.
  const input1 = wrapper.find(".code");
  const input2 = wrapper.find(".name");
  const addMovements = wrapper.find(".btnAddCity");
  input1.setValue("1");
  input2.setValue("tanger");

  expect(wrapper.vm.cities.id_city).toBe("1");
  expect(wrapper.vm.cities.name_city).toBe("tanger");
});

test("si funciona la function Añadir ciudades", async () => {
  api.addCities = jest.fn();
  const mockCities = [
    {
      id_city: "1",
      name_city: "Tanger",
    },
  ];
  api.addCities.mockReturnValue(mockCities);
  const wrapper = shallowMount(Addcity, {
    data() {
      return {};
    },
  });
  const submit = wrapper.find(".btnAddCity");
  submit.trigger("click");
  await finishAsyncTasks();
  expect(api.addCities).toHaveBeenCalled();
});
