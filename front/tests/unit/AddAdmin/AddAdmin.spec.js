import { shallowMount } from "@vue/test-utils";
import Addadmin from "@/components/Addadmin/Addadmin.vue";
//import api from '@/api.js'
import api from "@/api.js";
const finishAsyncTasks = () => new Promise(setImmediate);

test("the input are admin ", async () => {
  api.getadmin = jest.fn();
  const wrapper = shallowMount(Addadmin, {
    data() {
      return {
        admin: {
          id_admin: "1",
          name_admin: "mohammed sehili",
          Email_admin: "med95sehili@gmail.com",
          Pssword_admin: "123",
        },
      };
    },
  });
  expect(wrapper.emitted().search).toBe(undefined);
  // Simular lo que hace el usuario.
  const input1 = wrapper.find(".code");
  const input2 = wrapper.find(".name");
  const input3 = wrapper.find(".email");
  const input4 = wrapper.find(".password");
  const addMovements = wrapper.find(".btnAddadmin");
  input1.setValue("1");
  input2.setValue("mohammed sehili");
  input3.setValue("med95sehili@gmail.com");
  input4.setValue("123");

  expect(wrapper.vm.admin.id_admin).toBe("1");
  expect(wrapper.vm.admin.name_admin).toBe("mohammed sehili");
  expect(wrapper.vm.admin.Email_admin).toBe("med95sehili@gmail.com");
  expect(wrapper.vm.admin.Pssword_admin).toBe("123");
});

test("si funciona la function Añadir admin", async () => {
  api.getadmin = jest.fn();
  const mocadmin = [
    {
      id_admin: "1",
      name_admin: "mohammed sehili",
      Email_admin: "med95sehili@gmail.com",
      Pssword_admin: "123",
    },
  ];
  api.getadmin.mockReturnValue(mocadmin);
  const wrapper = shallowMount(Addadmin, {
    data() {
      return {};
    },
  });
  const submit = wrapper.find(".btnAddadmin");
  submit.trigger("click");
  await finishAsyncTasks();
  expect(api.getadmin).toHaveBeenCalled();
});
