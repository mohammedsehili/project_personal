import { shallowMount } from '@vue/test-utils'
import Login from '@/components/Login/Login.vue'

import api from '@/api.js'

//import { finishAllAsyncTasks, mockRouter } from './helpers'

function mountPage() {
    return shallowMount(Login, {
        mocks: {
            $router: mockRouter,
        },
    })
}

test.skip('call api login ', async () => {
    api.login = jest.fn().mockResolvedValue({
        ok: true,
    })

    const wrapper = mountPage()

    wrapper.vm.mailLogin = 'email@example.com'
    wrapper.vm.passwordLogin = 'password'

    wrapper.find('button').trigger('click')
    await finishAllAsyncTasks()

    expect(api.login).toHaveBeenCalledWith('email@example.com', 'password')
    expect(wrapper.vm.$router.path).toBe('/ViewCities')
})
