import { shallowMount } from "@vue/test-utils";
import AdminItem from "@/components/Admin/AdminItem.vue";
import Admin from "@/components/Admin/Admin.vue";

test("pinta todo list vacío", () => {
  const wrapper = shallowMount(AdminItem, {
    propsData: {
      admin: [],
    },
  });
  const table_Admin = wrapper.findAll(Admin).wrappers;

  expect(table_Admin.length).toBe(0);
});

test("Este el Button de Eliminar ", async () => {
  const admin = {
    id_Admin: "01",
    name_Admin: "med",
    Email_Admin: "email@gmail.com",
    Pssword_Admin: "123",
  };
  const wrapper = shallowMount(AdminItem, {
    propsData: { admin: admin },
  });
  expect(wrapper.emittedByOrder()).toEqual([]);

  const button = wrapper.find(".btndelete");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    { name: "adminDelete", args: [admin.idadmin] },
  ]);
});
