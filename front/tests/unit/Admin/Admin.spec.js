import { shallowMount } from "@vue/test-utils";
import Admin from "@/components/Admin/Admin.vue";

const finishAsyncTasks = () => new Promise(setImmediate);

test("si funciona la function Ciudades ", async () => {
  const MockApiCall = jest.fn();
  const mockAdmin = [
    {
      id_Admin: "01",
      name_Admin: "med",
      Email_Admin: "email@gmail.com",
      Pssword_Admin: "123",
    },
  ];
  MockApiCall.mockReturnValue(mockAdmin);
  const wrapper = shallowMount(Admin, {
    methods: {
      displayAdmin: MockApiCall,
    },
  });

  await finishAsyncTasks();
  expect(MockApiCall).toHaveBeenCalled();
});
