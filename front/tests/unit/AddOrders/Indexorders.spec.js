import { shallowMount } from "@vue/test-utils";
import Indexorders from "@/components/AddOrders/Indexorders.vue";
import api from "@/api.js";
const finishAsyncTasks = () => new Promise(setImmediate);

test("the input are añadir orders ", async () => {
  api.getOrders = jest.fn();
  const wrapper = shallowMount(Indexorders, {
    data() {
      return {
        orders: {
          name_orders: "mohammed",
          email_orders: "test@gmail.com",
          phone_ordres: "86822364532",
          notes_ordres: "no se",
        },
      };
    },
  });
  expect(wrapper.emitted().search).toBe(undefined);
  // Simular lo que hace el usuario.
  const input1 = wrapper.find(".name");
  const input2 = wrapper.find(".email");
  const input3 = wrapper.find(".phone");
  const input4 = wrapper.find(".message");

  const addMovements = wrapper.find(".btnAddOrders");
  input1.setValue("mohammed");
  input2.setValue("test@gmail.com");
  input3.setValue("86822364532");
  input4.setValue("no se");

  expect(wrapper.vm.orders.name_orders).toBe("mohammed");
  expect(wrapper.vm.orders.email_orders).toBe("test@gmail.com");
  expect(wrapper.vm.orders.phone_ordres).toBe("86822364532");
  expect(wrapper.vm.orders.notes_ordres).toBe("no se");
});
test.skip("si funciona la function Añadir orders", async () => {
  api.getOrders = jest.fn();
  const mockorders = [
    {
      nameService: "test",
      nameCity: "bilbao",
      name_orders: "mohammed",
      email_orders: "test@gmail.com",
      phone_ordres: "86822364532",
      notes_ordres: "no se",
    },
  ];

  api.getOrders.mockReturnValue(mockorders);
  const wrapper = shallowMount(Indexorders, {
    methods: {
      ...Indexorders.methods,
      displaycity: api.getOrders,
      displayservices: api.getOrders,
    },
  });
  const submit = wrapper.find(".btnAddOrders");
  submit.trigger("click");
  await wrapper.vm.$nextTick();
  await finishAsyncTasks();
  expect(api.getOrders).toHaveBeenCalled();
  expect(wrapper.vm.orders).toBe(mockorders);
});
