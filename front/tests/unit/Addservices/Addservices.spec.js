import { shallowMount } from "@vue/test-utils";
import Addservices from "@/components/Addservices/Addservices.vue";

import api from "@/api.js";
const finishAsyncTasks = () => new Promise(setImmediate);

test("the input are Servicios ", async () => {
  api.addService = jest.fn();
  const wrapper = shallowMount(Addservices, {
    data() {
      return {
        service: {
          id_services: "",
          name_services: "",
        },
      };
    },
  });
  expect(wrapper.emitted().search).toBe(undefined);
  // Simular lo que hace el usuario.
  const input1 = wrapper.find(".code");
  const input2 = wrapper.find(".name");
  const addMovements = wrapper.find(".btNAddService");
  input1.setValue("1");
  input2.setValue("test");

  expect(wrapper.vm.service.id_services).toBe("1");
  expect(wrapper.vm.service.name_services).toBe("test");
});

test("si funciona la function Añadir Servicios", async () => {
  api.addService = jest.fn();
  const mockServices = [
    {
      id_services: "1",
      name_services: "test",
    },
  ];
  api.addService.mockReturnValue(mockServices);
  const wrapper = shallowMount(Addservices, {
    data() {
      return {};
    },
  });
  const submit = wrapper.find(".btNAddService");
  submit.trigger("click");
  await finishAsyncTasks();
  expect(api.addService).toHaveBeenCalled();
});
