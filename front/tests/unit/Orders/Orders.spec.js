import { shallowMount } from "@vue/test-utils";
import Orders from "@/components/Orders/Orders.vue";

const finishAsyncTasks = () => new Promise(setImmediate);

test("si funciona la function Orders ", async () => {
  const MockApiCall = jest.fn();
  const mockOrders = [
    {
      id_oreders: "1",
      nameService: "test",
      nameCity: "tanger",
      name_orders: "med",
      email_orders: "med@gmail.com",
      phone_ordres: "6854235",
      notes_ordres: "test",
    },
  ];
  MockApiCall.mockReturnValue(mockOrders);
  const wrapper = shallowMount(Orders, {
    methods: {
      displayOrders: MockApiCall,
    },
  });

  await finishAsyncTasks();
  expect(MockApiCall).toHaveBeenCalled();
});
