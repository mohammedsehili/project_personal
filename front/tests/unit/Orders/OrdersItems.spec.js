import { shallowMount } from '@vue/test-utils'
import Orders from '@/components/Orders/Orders.vue'
import OrdersItems from '@/components/Orders/OrdersItems.vue'

test('pinta todo list vacío', () => {
    const wrapper = shallowMount(OrdersItems, {
        propsData: {
            orders: [],
        },
    })
    const Table_Orders = wrapper.findAll(Orders).wrappers

    expect(Table_Orders.length).toBe(0)
})

test('Este el Button de Eliminar ', async () => {
    const orders = {
        idorders: 1,
        id_oreders: 1,
        nameService: 'test',
        nameCity: 'tanger',
        name_orders: 'mohammed',
        email_orders: 'mohammed@gmail.com',
        phone_ordres: 58248622,
        notes_ordres: 'yyyyyyyyyy',
    }
    const wrapper = shallowMount(OrdersItems, {
        propsData: { orders: orders },
    })
    expect(wrapper.emittedByOrder()).toEqual([])

    const button = wrapper.find('.btndeleteorders')
    button.trigger('click')
    await wrapper.vm.$nextTick()

    console.log('emmitedByOrder', wrapper.emittedByOrder())
    expect(wrapper.emittedByOrder()).toEqual([
        { name: 'ordersDelete', args: [orders.idorders] },
    ])
})
