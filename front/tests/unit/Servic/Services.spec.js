import { shallowMount } from "@vue/test-utils";
import Services from "@/components/Servic/Services.vue";

const finishAsyncTasks = () => new Promise(setImmediate);

test("Mostrar la tabla de Servicios ", async () => {
  const MockApiCall = jest.fn();
  const mockServices = [
    {
      id_services: "1",
      name_services: "test1",
    },
    {
      id_services: "2",
      name_services: "test2",
    },
  ];
  MockApiCall.mockReturnValue(mockServices);
  const wrapper = shallowMount(Services, {
    methods: {
      displayServices: MockApiCall,
    },
  });

  await finishAsyncTasks();
  expect(MockApiCall).toHaveBeenCalled();
});
