import { shallowMount } from '@vue/test-utils'
import Services from '@/components/Servic/Services.vue'
import Servicesitms from '@/components/Servic/Servicesitms.vue'
test('pinta todo list vacío', () => {
    const wrapper = shallowMount(Servicesitms, {
        propsData: {
            services: [],
        },
    })
    const Table_Service = wrapper.findAll(Services).wrappers

    expect(Table_Service.length).toBe(0)
})

test('Este el Button de Eliminar ', async () => {
    const services = {
        idservice: '1',
        id_services: 'A11',
        name_services: 'Psicólogos',
    }
    const wrapper = shallowMount(Servicesitms, {
        propsData: { services: services },
    })
    expect(wrapper.emittedByOrder()).toEqual([])

    const button = wrapper.find('.btnDeleteService')
    button.trigger('click')
    await wrapper.vm.$nextTick()

    console.log('emmitedByOrder', wrapper.emittedByOrder())
    expect(wrapper.emittedByOrder()).toEqual([
        { name: 'ServiceDelete', args: [services.idservice] },
    ])
})
