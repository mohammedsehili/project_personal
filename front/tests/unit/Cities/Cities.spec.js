import { shallowMount } from "@vue/test-utils";
import Cities from "@/components/Cities/Cities.vue";

const finishAsyncTasks = () => new Promise(setImmediate);

test("si funciona la function Ciudades ", async () => {
  const MockApiCall = jest.fn();
  const mockCity = [
    {
      id: "01",
      name: "tanger",
    },
    {
      id: "02",
      name: "bilbao",
    },
  ];
  MockApiCall.mockReturnValue(mockCity);
  const wrapper = shallowMount(Cities, {
    methods: {
      displaycity: MockApiCall,
    },
  });

  await finishAsyncTasks();
  expect(MockApiCall).toHaveBeenCalled();
});
