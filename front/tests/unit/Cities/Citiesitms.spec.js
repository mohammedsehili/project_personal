import { shallowMount } from "@vue/test-utils";
import Cities from "@/components/Cities/Cities.vue";
import Citiesitms from "@/components/Cities/Citiesitms.vue";

test("pinta todo list vacío", () => {
  const wrapper = shallowMount(Citiesitms, {
    propsData: {
      city: [],
    },
  });
  const table_Cities = wrapper.findAll(Cities).wrappers;

  expect(table_Cities.length).toBe(0);
});

test("Este el Button de Eliminar ", async () => {
  const city = {
    idcity: "1",
    name_city: "Tanger",
  };
  const wrapper = shallowMount(Citiesitms, {
    propsData: { city: city },
  });
  expect(wrapper.emittedByOrder()).toEqual([]);

  const button = wrapper.find(".btnDelete");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    { name: "cityDelete", args: [city.idcity] },
  ]);
});
